import osa
import os.path

BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def wsdl_client(wsdl_url):
    client = osa.Client(wsdl_url)
    return client


def temp_convert(fromUnit, toUnit, amount):
    url = 'http://www.webservicex.net/ConvertTemperature.asmx?WSDL'
    temp_unit = {
        'C' : 'degreeCelsius',
        'F': 'degreeFahrenheit',
    }
    client = wsdl_client(url)
    result = client.service.ConvertTemp(Temperature=amount, FromUnit=temp_unit[fromUnit], ToUnit=temp_unit[toUnit])
    return result


def convert_currency(fromUnit, toUnit, amount):
    url = 'http://fx.currencysystem.com/webservices/CurrencyServer4.asmx?WSDL'
    client = wsdl_client(url)
    result = client.service.ConvertToNum(toCurrency=toUnit, fromCurrency=fromUnit, amount=amount, rounding=True)
    return result


def convert_distance(fromUnit, toUnit, amount):
    url = 'http://www.webservicex.net/length.asmx?WSDL'
    distance_unit = {
        'km': 'Kilometers',
        'mi': 'Miles',
    }
    client = wsdl_client(url)
    result = client.service.ChangeLengthUnit(LengthValue=amount,
                                             fromLengthUnit=distance_unit[fromUnit],
                                             toLengthUnit=distance_unit[toUnit])
    return result

def fetch_avg_temp_from_file(f_name):
    f_name = os.path.join(BASE_DIR, f_name)
    temp_all = 0
    with open(f_name, 'r', encoding='utf-8') as file:
        lines = file.readlines()
        for line in lines:
            temp, unit = line.strip().split(' ')
            temp_all += float(temp)
    temp_avg = temp_all / len(lines)
    result = temp_convert(unit, 'C', temp_avg)
    print('Средняя температура по Цельсию: {:.2f}'.format(result))



def read_currencies_file(f_name):
    f_name = os.path.join(BASE_DIR, f_name)
    price_all = 0
    with open(f_name, 'r', encoding='utf-8') as file:
        lines = file.readlines()
        for line in lines:
            route, price, currency = line.strip().split(' ')
            price = convert_currency(currency, 'RUB', price)
            price_all += price
            # print('Для маршрута {} необходимо {:.0f} RUB'.format(route, price))
    print('Итоговая сумма затрат {:.0f} RUB'.format(price_all))


def read_travel_file(f_name):
    f_name = os.path.join(BASE_DIR, f_name)
    distance_all = 0
    with open(f_name, 'r', encoding='utf-8') as file:
        lines = file.readlines()
        for line in lines:
            route, distance, unit = line.strip().split(' ')
            distance = str(distance).replace(',', '')
            distance = convert_distance(unit, 'km', distance)
            distance_all += float(distance)
            # print('Длинна маршрута {} {:.2f} km'.format(route, distance))
    print('Итоговая длинна маршрута {:.2f} km'.format(distance_all))


fetch_avg_temp_from_file('temps.txt')
print('==================================================================================================')
read_currencies_file('currencies.txt')
print('==================================================================================================')
read_travel_file('travel.txt')
